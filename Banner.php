<?php


class Banner {

	private $source = "banner.jpg";

	
	public function CreateBanner($data) {
		
		
	$game_days_pvp = floor($data['data']['pvp']['totalBattleTime']/1000/60/60/24);
	$game_hour_pvp = floor(($data['data']['pvp']['totalBattleTime'] - $game_days_pvp*1000*60*60*24)/1000/60/60);
	$game_minutes_pvp = floor(($data['data']['pvp']['totalBattleTime'] - $game_days_pvp*1000*60*60*24 - $game_hour_pvp*1000*60*60)/1000/60);
	$game_sec_pvp = floor(($data['data']['pvp']['totalBattleTime'] - $game_days_pvp*1000*60*60*24 - $game_hour_pvp*1000*60*60 - $game_minutes_pvp*1000*60)/1000); 
	$game_msec_pvp = floor(($data['data']['pvp']['totalBattleTime'] - $game_days_pvp*1000*60*60*24 - $game_hour_pvp*1000*60*60 - $game_minutes_pvp*1000*60 - $game_sec_pvp*1000));
	$kd = $data['data']['pvp']['totalKill']/$data['data']['pvp']['totalDeath'];
		
		$image  	= imagecreatefromjpeg($this->source);
		
		$Font	= '24Janvier-Light.ttf';
				
		$color	= imagecolorallocate($image, 255, 255, 225);
		$shadow = imagecolorallocate($image, 33, 33, 33);
		
		$POS = array(array(15, 0, 50, 31), array(15, 0, 50, 30), array(15, 0, 190, 31));
		//var_dump($POS);
		
		$wl	= $data['data']['pvp']['gameWin'] / ($data['data']['pvp']['gamePlayed'] - $data['data']['pvp']['gameWin']);
		
		
		imagettftext($image, 15, 0, 50, 31, $shadow, $Font, $data['data']['clan']['tag'].' '.$data['data']['nickName']);
		imagettftext($image, 15, 0, 50, 30, $color, $Font, $data['data']['clan']['tag'].' '.$data['data']['nickName']);
		
		//imagettftext($image, 14, 0, 180, 31, $shadow, $Font, 'Kämpfe: '.$data['data']['pvp']['gamePlayed']);
		//imagettftext($image, 14, 0, 180, 30, $color, $Font, 'Kämpfe: '.$data['data']['pvp']['gamePlayed']);
		
		imagettftext($image, 14, 0, 180, 31, $shadow, $Font, 'KD: '.number_format($kd, 1));
		imagettftext($image, 14, 0, 180, 30, $color, $Font, 'KD: '.number_format($kd, 1));
		
		imagettftext($image, 14, 0, 180, 56, $shadow, $Font, 'Siege: '.$data['data']['pvp']['gameWin']);
		imagettftext($image, 14, 0, 180, 55, $color, $Font, 'Siege: '.$data['data']['pvp']['gameWin']);
		
		imagettftext($image, 14, 0, 180, 81, $shadow, $Font, 'S/N-QUOTE: '.number_format($wl, 2));
		imagettftext($image, 14, 0, 180, 80, $color, $Font, 'S/N-QUOTE: '.number_format($wl, 2));
		
		imagettftext($image, 14, 0, 50, 56, $shadow, $Font, "Karma: " .$data['data']['karma']);
		imagettftext($image, 14, 0, 50, 55, $color, $Font, "Karma: " .$data['data']['karma']);
				
		imagettftext($image, 14, 0, 50, 81, $shadow, $Font, "Elo: " .number_format($data['data']['effRating'], 0,'.',''));
		imagettftext($image, 14, 0, 50, 80, $color, $Font, "Elo: " .number_format($data['data']['effRating'], 0,'.',''));	

	//	imagettftext($image, 14, 0, 50, 81, $color, $Font, "" .number_format($game_days_pvp, 0)."d ".number_format($game_hour_pvp, 0)."h ".number_format($game_minutes_pvp, 0)."min ".number_format($game_sec_pvp, 0)."sec");
	//	imagettftext($image, 14, 0, 50, 80, $color, $Font, "" .number_format($game_days_pvp, 0)."d ".number_format($game_hour_pvp, 0)."h ".number_format($game_minutes_pvp, 0)."min ".number_format($game_sec_pvp, 0)."sec ".$game_msec_pvp);
		
			
		if(!isset($_GET['debug']))
		{
				header('Content-type: image/jpg');
			
		}		
		
		ImageJPEG($image);
		imagedestroy($image);
	}
}
